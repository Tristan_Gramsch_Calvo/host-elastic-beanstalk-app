# Build the image and set AWS credentials
FROM amazonlinux

# Update package information and install necessary tools
RUN yum update -y && \
    yum install -y aws-cli nodejs && \
    npm install -g aws-cdk && \
    mkdir eb-app

# Create the AWS config file with credentials
RUN mkdir -p ~/.aws && \
    echo "[profile my-dev-profile]" >> ~/.aws/config && \
    echo "sso_session = first" >> ~/.aws/config && \
    echo "sso_account_id = 485436010131" >> ~/.aws/config && \
    echo "sso_role_name = AdministratorAccess" >> ~/.aws/config && \
    echo "region = us-west-2" >> ~/.aws/config && \
    echo "output = json" >> ~/.aws/config && \
    echo "[sso-session first]" >> ~/.aws/config && \
    echo "sso_start_url = https://d-90679d4783.awsapps.com/start#" >> ~/.aws/config && \
    echo "sso_region = us-east-1" >> ~/.aws/config && \
    echo "sso_registration_scopes = sso:account:access" >> ~/.aws/config

# Set the entrypoint to /bin/bash
ENTRYPOINT ["/bin/bash"]

# Run
# docker run --tty -i aws-runner
# aws sso login --profile my-dev-profile
# cdk init app --language typescript 
# cdk deploy --profile my-dev-profile
# cdk destroy --profile my-dev-profile

