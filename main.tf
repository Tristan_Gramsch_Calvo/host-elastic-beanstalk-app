terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 4.16"
        }
    }
    
    required_version = ">= 1.2.0"
}

provider "aws" {
    region = "us-east-1"
}

import {
    to = aws_elastic_beanstalk_application.docker_app
    id = "test-docker"
}

resource "aws_elastic_beanstalk_application" "docker_app" {
  name        = "test-docker"
  description = "First docker app"
}

import {
    to = aws_elastic_beanstalk_environment.example
    id = "e-yxvhdpg5p2"
}

resource "aws_elastic_beanstalk_environment" "example" {
    name = "Test-docker-env"
    application = aws_elastic_beanstalk_application.docker_app.name
}