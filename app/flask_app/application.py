from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        # Retrieve form data
        race = request.form["race"]
        age = request.form["age"]
        gender = request.form["gender"]
        socioeconomic_status = request.form["socioeconomic_status"]

        # Determine the risk of violence based on some arbitrary criteria
        # Replace this with real risk assessment factors in a real application
        at_risk = (race == "White" and age < "25") or (gender == "Male" and socioeconomic_status == "Low")

        # Return the message based on the risk assessment
        message = "At risk of violence" if at_risk else "Not at risk of violence"
        return render_template("result.html", message=message)

    # Render the form template for GET requests
    return render_template("form.html")

if __name__ == "__main__":
    app.run(port = 5000, debug=True, host='0.0.0.0')
